//
//  GameViewController.swift
//  BallJump
//
//  Created by Francesco Stabile on 27/03/2019.
//  Copyright © 2019 Francesco Stabile. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import CoreMotion
class GameViewController: UIViewController {
    
    var motionManager = CMMotionManager()
    var scene: GameScene!
    
    var changeProperty = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .up
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .down
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .right
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        view.addGestureRecognizer(upSwipe)
        view.addGestureRecognizer(downSwipe)
        
        
        //        Double Tap To Change Brush Color
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTap.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTap)
        
        //        Pinch Gesture
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch(sender:)))
        view.addGestureRecognizer(pinch)
        
        GyroScopeController()
        if let view = self.view as! SKView? {
            scene = (SKScene(fileNamed: "GameScene") as! GameScene)
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .aspectFill
            
            // Present the scene
            view.presentScene(scene)
            
            
            view.ignoresSiblingOrder = true
            
        }
        
        scene?.changeLineColor()
        scene?.changeColorBack()
        
        self.activateProximitySensor()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func GyroScopeController()
    {
        var prevState:Double = 0
        motionManager.accelerometerUpdateInterval = 0.001
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!, withHandler: {(data , error) in
            if let myData = data
            {
                let force = myData.acceleration.z
                if (force > 0.1)
                {
                    self.scene.JumpBall(yVector: CGFloat(myData.acceleration.y), xVector: CGFloat(myData.acceleration.x))
                    prevState = myData.acceleration.z
                }
                else
                {
                    self.scene.MoveBall(yVector: CGFloat(myData.acceleration.y), xVector: CGFloat(myData.acceleration.x))
                }
            }
        })
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        scene?.changeColorBack()
    }
    
    @objc func doubleTapped(_ sender:UITapGestureRecognizer)
    {
        scene?.changeLineColor()
    }
    
    var previosSize: CGFloat = 0.0
    
    @objc func handlePinch(sender: UIPinchGestureRecognizer)
    {
        guard sender.view != nil && scene.isOnTheGround else { return }
        
        if sender.state == UIGestureRecognizer.State.changed {
            let screenDimesion = min(self.view.frame.width, self.view.frame.height)
            let minBallSize = screenDimesion/12
            let maxBallSize = screenDimesion/1.5
            
            let scale = sender.scale
            let newBallSize = scene.ball.frame.width * scale
            
            if (newBallSize < minBallSize)
            {
                scene.ballOnTheGroundSize = CGSize(width: minBallSize, height: minBallSize)
                self.scene.ball.scale(to: scene.ballOnTheGroundSize)
                //                print(scene.ballOnTheGroundSize)
                return
            }
            if (newBallSize > maxBallSize)
            {
                scene.ballOnTheGroundSize = CGSize(width: maxBallSize, height: maxBallSize)
                self.scene.ball.scale(to: scene.ballOnTheGroundSize)
                //                print(scene.ballOnTheGroundSize)
                return
            }
            
            scene.ballOnTheGroundSize = CGSize(width: (scene.ball.frame.width * scale), height: (scene.ball.frame.height * scale))
            //            print(scene.ballOnTheGroundSize)
            scene.ball.scale(to: scene.ballOnTheGroundSize)
        }
    }
    
    func activateProximitySensor() {
        let device = UIDevice.current
        device.isProximityMonitoringEnabled = true
        
        if device.isProximityMonitoringEnabled
        {
            NotificationCenter.default.addObserver(self, selector: #selector(proximityChanged), name: NSNotification.Name(rawValue: "UIDeviceProximityStateDidChangeNotification"), object: device)
        }
        else
        {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(tapAndHold))
            view.addGestureRecognizer(longPress)
        }
        
    }
    
    @objc func tapAndHold(_ sender: UILongPressGestureRecognizer)
    {
        cleanPage()
    }
    
    
    @objc func proximityChanged(notification: NSNotification) {
        if (notification.object as? UIDevice) != nil {
            cleanPage()
        }
    }
    
    func cleanPage()
    {
        if changeProperty {
            scene?.children.forEach {
                if $0.name == scene?.LineNodeName {
                    $0.removeFromParent()
                }
            }
            changeProperty = false
        }
        else {
            changeProperty = true
        }
    }
    
}
