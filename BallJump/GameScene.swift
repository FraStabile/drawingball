//
//  GameScene.swift
//  BallJump
//
//  Created by Francesco Stabile on 27/03/2019.
//  Copyright © 2019 Francesco Stabile. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    let ballMovementSpeed: CGFloat = 8.0
    let jumpPause = 0.11
    var ballOnTheGroundSize = CGSize(width: 50, height: 50)
    
    var lineColor: UIColor!
    var ball: SKSpriteNode!
    var isOnTheGround = true
    var alphaFactor: CGFloat = 0.0
    
    let ColorRange = [UIColor.gray, UIColor.white, UIColor.red, UIColor.blue, UIColor.yellow]
    var indexBackColor = -1
    var indexLineColor = 0
    var LineNodeName = "LineNode"
    
    override func sceneDidLoad() {
        ball = SKSpriteNode(texture: SKTexture(image: UIImage(named: "ball")!))
        ball.position = CGPoint(x: (scene?.frame.midX)!, y: (scene?.frame.midY)!)
        ball.size = ballOnTheGroundSize
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.frame.height)
        ball.physicsBody?.affectedByGravity = false
        scene?.addChild(ball)        
    }
    
    func JumpBall(yVector: CGFloat, xVector: CGFloat)
    {
        if isOnTheGround == false {
            return
        }
        isOnTheGround = false
        let jumpIn = SKAction.resize(toWidth: ballOnTheGroundSize.width * 3, height: ballOnTheGroundSize.height * 3, duration: 0.6)
        let wait = SKAction.wait(forDuration: jumpPause)
        let jumpOff = SKAction.resize(toWidth: ballOnTheGroundSize.width, height: ballOnTheGroundSize.height, duration: 0.6)
        let movement = SKAction.move(by: CGVector(dx: xVector * 100, dy: yVector * 100), duration: 0.5)
        let jumpAction = SKAction.sequence([jumpIn, wait, jumpOff])
        ball.run(jumpAction, completion: {() -> Void in
            print(self.ballOnTheGroundSize)
            self.ball.size = self.ballOnTheGroundSize
            print(self.ball.size)
            self.isOnTheGround = true
            self.alphaFactor = 0.0
        })
        ball.run(movement)
    }
    
    func MoveBall(yVector: CGFloat, xVector: CGFloat)
    {
        let movement = SKAction.move(by: CGVector(dx: xVector * ballMovementSpeed, dy: yVector * ballMovementSpeed), duration: 0.5)
        ball.run(movement)
        
        if isOnTheGround {
            let line = drawLine(fromPoint: ball.position, 1.0 - alphaFactor)
            line.name = LineNodeName
            alphaFactor += 0.001
            scene?.addChild(line)
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    func drawLine(fromPoint: CGPoint, _ alpha: CGFloat) -> SKShapeNode {
        let yourline = SKShapeNode()
        let pathToDraw = CGMutablePath()
        pathToDraw.move(to: fromPoint)
        pathToDraw.addLine(to: fromPoint)
        
        yourline.path = pathToDraw
        yourline.lineCap = .round
        yourline.lineWidth = ballOnTheGroundSize.width / 2.5
        //        yourline.strokeColor = SKColor.init(displayP3Red: 255.0, green: 255.0, blue: 255.0, alpha: alpha)
        
        
        yourline.strokeColor = lineColor.withAlphaComponent(alpha)
        
        yourline.blendMode = SKBlendMode(rawValue: Int(CGBlendMode.normal.rawValue))!
        return yourline
    }
    
    func changeColorBack()
    {
        indexBackColor = (indexBackColor + 1)%ColorRange.count
        
        if indexBackColor == indexLineColor
        {
            indexBackColor = (indexBackColor + 1)%ColorRange.count
        }
        
        scene?.backgroundColor = ColorRange[indexBackColor]
    }
    
    func changeLineColor()
    {
        indexLineColor = (indexLineColor + 1)%ColorRange.count
        
        if indexLineColor == indexBackColor
        {
            indexLineColor = (indexLineColor + 1)%ColorRange.count
        }
        
        self.lineColor = ColorRange[indexLineColor]
    }
    
}
